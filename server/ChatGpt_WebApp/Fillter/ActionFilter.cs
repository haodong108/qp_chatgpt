using System.Net;
using ChatGpt_WebApp.ApiDto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace ChatGpt_WebApp.Fillter;

public class ActionFilter : IActionFilter
{
    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (!context.ModelState.IsValid) // 是否通过验证
        {
            var result = context.ModelState.Keys
                .SelectMany(key => context.ModelState[key]!.Errors.Select(x => new ValidationErrorDto(key, x.ErrorMessage)))
                .ToList();
            context.Result = new ObjectResult(new ApiResult
            {
                Code = 400,
                Message = "验证失败",
                Data = result,
            });
        }
    }

    public void OnActionExecuting(ActionExecutingContext context)
    {
            
    }
}
