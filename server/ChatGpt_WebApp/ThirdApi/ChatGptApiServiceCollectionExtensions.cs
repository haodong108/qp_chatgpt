using ChatGPT.Net.DTO;
using ChatGpt_WebApp.ThirdApi;
/// <summary>
/// ChatGpt会话AI 服务扩展
/// </summary>
public static class ChatGptApiServiceCollectionExtensions
{
    public static IServiceCollection AddChatGptApi(this IServiceCollection services, Action<ApiConfig> config)
    {
        var options = new ApiConfig();
        config(options);        

        services.AddSingleton(options);

        services.AddSingleton<IChatApi, PawanReverseApi>();

        return services;
    }
}

/// <summary>
/// 
/// </summary>
public class ApiConfig
{   

    /// <summary>
    /// 官网 SessionToken
    /// </summary>
    /// <value></value>
    public string SessionToken { get; set; } = "";

    /// <summary>
    /// 缓存配置文件目录
    /// </summary>
    /// <value></value>
    public string ConfigDir { get; set; } = "";

    /// <summary>
    /// 是否是Plus版本
    /// </summary>
    /// <value></value>
    public bool IsPlus { get; set; } = false;

    /// <summary>
    /// API Key
    /// </summary>
    /// <value></value>
    public string ApiKey { get; set; } = "";


    public ApiConfig()
    {
       
    }
}