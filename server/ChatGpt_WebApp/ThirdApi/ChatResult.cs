﻿using System;

namespace ChatGpt_WebApp.ThirdApi
{
    /// <summary>
    /// ChatGpt的回复结果
    /// </summary>
    public class ChatResult
    {
        /// <summary>
        /// 消息回复内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; } = false;

        /// <summary>
        /// 发生的异常
        /// </summary>
        public System.Exception? Exception { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public ChatResultStatus Status { get; set; }
    }

    
    public enum ChatResultStatus
    {
        /// <summary>
        /// 未知原因
        /// </summary>
        Unknown = 0,
        
        /// <summary>
        /// 正常返回
        /// </summary>
        Success = 1,

        /// <summary>
        /// 响应超时
        /// </summary>
        TimeOut = 2,

        /// <summary>
        /// 连接被关闭
        /// </summary>
        ConnectClosed = 3,

        /// <summary>
        /// 问题过长
        /// </summary>
        QuestionTooLong = 4,
    }
}
