using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatGPT.Net.DTO;

namespace ChatGpt_WebApp.ThirdApi
{
    public abstract class ChatApiBase : IChatApi
    {   
        protected readonly  ApiConfig apiContext;
        protected readonly  AppSetting.ChatGptConfig gptConfig = AppSetting.Current.ChatGpt;
        protected readonly Microsoft.Extensions.Logging.ILogger logger;        

        public ChatApiBase(ILogger logger,ApiConfig config)
        {
            this.apiContext = config;
            this.logger = logger;
        }

        public abstract Task<ChatResult> Ask(string question, string conversationId);
        
        public virtual Task<string> AskStream(Action<string> callback, string prompt, string? conversationId = null){
              throw new NotImplementedException();   
        }

        public abstract Task ResetConversation(string conversationId);
    }
}