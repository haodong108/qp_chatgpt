﻿namespace ChatGpt_WebApp.ThirdApi
{
    /// <summary>
    /// 会话AI通用接口
    /// </summary>
    public interface IChatApi
    {
        /// <summary>
        /// 向AI提问
        /// </summary>
        /// <typeparam name="T">返回类型</typeparam>
        /// <param name="question">问题</param>
        /// <param name="conversationId">会话ID</param>
        /// <returns></returns>
        public Task<ChatResult> Ask(string question, string conversationId);        

         /// <summary>
        /// 询问(流式)
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="prompt"></param>
        /// <param name="conversationId"></param>
        /// <returns></returns>
        public Task<string> AskStream(Action<string> callback, string prompt, string? conversationId = null);

        /// <summary>
        /// 清理会话
        /// </summary>
        /// <param name="conversationId">会话ID</param>
        public Task ResetConversation(string conversationId);       
        

        
    }
}
