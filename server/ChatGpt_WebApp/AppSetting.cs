using System.Runtime;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NLog;

namespace ChatGpt_WebApp;

/// <summary>
/// 程序配置
/// </summary>
public class AppSetting
{
    static IConfiguration? _config = null;

    private static AppSetting lastSetting = new AppSetting();
    public static AppSetting Current
    {
        get
        {
            if (_config == null) throw new Exception("配置尚未初始化");
            try
            {
                lastSetting = new AppSetting(_config);                
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Log(NLog.LogLevel.Error, e);
            }
            return lastSetting;
        }
    }

    /// <summary>
    /// 管理员IP列表
    /// </summary>
    public List<string> AdminIps { get; set; } = new List<string>();

    /// <summary>
    /// ChatGpt配置
    /// </summary>
    public ChatGptConfig ChatGpt { get; private set; } = new ChatGptConfig();

    public AppSetting()
    {
        
    }

    public AppSetting(IConfiguration config)
    {
        AdminIps = config.GetSection("AdminIps").Get<List<string>>()!;

        var gptSection = config.GetSection("ChatGpt");
        this.ChatGpt = gptSection.Get<ChatGptConfig>()!;
        //gptSection.Bind(this.ChatGpt);
    }

    /// <summary>
    /// 初始化
    /// <summary>
    public static void Init(IConfiguration config)
    {
        _config = config;
    }
   

    /// <summary>
    /// ChatGpt配置项
    /// </summary>
    public class ChatGptConfig
    {
        /// <summary>
        /// AI名称
        /// </summary>
        public string AiName { get; set; } = "小白猿";

        /// <summary>
        /// 应用版本号
        /// </summary>
        public string Version { get; set; } = "1.0.0";

        /// <summary>
        /// 问题最大长度
        /// </summary>
        public int MaxQuesionLength { get; set; }

        /// <summary>
        /// gpt回答问题的参考长度，数值太高会更加消耗api额度,但回答会更加全面
        /// </summary>
        /// <value></value>
        public int MaxTokenLength { get; set; } = 1024;

        /// <summary>
        /// 使用官方API
        /// </summary>
        /// <value></value>
        public bool UseOfficialApi { get; set; } = true;

        /// <summary>
        /// 海外openai代理服务器
        /// </summary>
        /// <value></value>
        public string OverseasProxy {get;set;} = "https://api.openai.com";

        /// <summary>
        /// Token池，用于多账号切换，避免限流
        /// </summary>
        public List<TokenItem> TokenPool { get; set; } = new List<TokenItem>();

        /// <summary>
        /// 要记录日志的IP
        /// </summary>
        public string[]? LogIps { get; set; } = new string[0];

        /// <summary>
        /// 性格模型
        /// </summary>
        public Character Character { get; set; } = new Character();

        /// <summary>
        /// 将Token池保存到配置文件中
        /// </summary>
        public void SaveTokenPool()
        {
            if (_config == null) throw new Exception("配置尚未初始化");
        }
    }
}

/// <summary>
/// 单个账号Token信息
/// </summary>
public class TokenItem
{
    /// <summary>
    /// 配置标识
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 账号
    /// </summary>
    public string Account { get; set; } = string.Empty;

    /// <summary>
    /// 账号密码
    /// </summary>
    public string Pwd { get; set; } = string.Empty;

    /// <summary>
    /// OpenAiKey
    /// </summary>
    public string ApiKey { get; set; } = string.Empty;

    /// <summary>
    /// Token
    /// </summary>
    public string SessionToken { get; set; } = string.Empty;

    /// <summary>
    /// 是否是Pro付费账号
    /// </summary>
    public bool IsProUser { get; set; }
}

/// <summary>
/// 性格
/// </summary>
public class Character
{
    /// <summary>
    /// Ai性格标注模板
    /// </summary>
    public string Template { get; set; } = "";

    /// <summary>
    /// 详细性格列表
    /// </summary>
    public List<CharacterInfo> Characters { get; private set; } = new List<CharacterInfo>();

}

/// <summary>
/// 性格信息
/// </summary>
public class CharacterInfo
{
    public static CharacterInfo Default = new CharacterInfo()
    {
        Id = 1,
        Desc = "原始的对话模型",
        Name = $"原始模型",
        AiDesc = "",
        Icon = ""
    };

    /// <summary>
    /// ID
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// 给AI看的描述
    /// </summary>
    public string? AiDesc { get; set; }

    /// <summary>
    /// 给用户看的描述
    /// </summary>
    public string? Desc { get; set; }

    /// <summary>
    /// 温度
    /// </summary>
    /// <value></value>
    public double Temperature { get; set; } = 0.7;

    /// <summary>
    /// 图标字体
    /// </summary>
    public string? Icon { get; set; }

    /// <summary>
    /// 头像地址
    /// </summary>
    /// <value></value>
    public string? HeadSrc { get; set; }

    /// <summary>
    /// 是否应该由Ai真正的打招呼
    /// </summary>
    public bool WelcomeFromAi { get; set; } = false;

    /// <summary>
    /// 欢迎用语列表
    /// </summary>
    public List<string> WelcomeMsgList { get; set; } = new List<string>();

    /// <summary>
    /// 清理后的会话随机列表
    /// </summary>
    public List<string> ReConversationMsgList { get; set; } = new List<string>();
}
