$(function () {
    window.layer = {
        msg: function (msg, options) {
            var options = options || {};
            var time = options.time || 2000;
            var baseDom = options.baseDom || $(".chat-view")[0];
            if ($(baseDom).css("position") == "static") {
                $(baseDom).css("position", "relative");
            }
            var msgDom = $(`
                    <div class="layer-msg show">
                        <span class="msg">${msg}</span>
                    </div>
            `);
            //关闭之前的消息框
            $(baseDom).find(".layer-msg").remove();
            $(baseDom).append(msgDom);

            msgDom.css({
                left: "calc(50% - " + msgDom.width() / 2 + "px)",
                top: `calc(50% - 60px)`,
            });

            var animationTime = parseFloat(msgDom.css("animation-duration")) * 1000;

            setTimeout(function () {
                //判断 msgDom 是否存在，如果不存在，说明已经被关闭了
                if (!msgDom[0]) return;
                msgDom.addClass("close");
                setTimeout(function () {
                    msgDom.remove();
                }, animationTime);
            }, time);
        },
    };

    //开启问题栏占比拖放
    (function enableQusionBoxDrag() {
        //鼠标拖放 quesion-panel 上边框时，改变quesion-panel 的height
        const btm = document.querySelector(".quesion-panel");
        const handle = btm.querySelector(".rezise");
        const chatpanel = $(".chat-view .chat-panel");

        let isResizing = false;
        let lastY = 0;
        let minHeight = $(btm).css("min-height").trimEnd("px");

        handle.addEventListener("mousedown", (e) => {
            isResizing = true;
            lastY = e.clientY;
            minHeight = parseInt($(btm).css("min-height").trimEnd("px"));
            maxHeight = parseInt($(btm).css("max-height").trimEnd("px"));
            //按下时禁用鼠标选中文字效果
            document.body.style.userSelect = "none";
        });

        document.addEventListener("mousemove", (e) => {
            if (!isResizing) return;

            const delta = e.clientY - lastY;
            const btmHeight = btm.getBoundingClientRect().height;
            var valueh = Math.max(btmHeight - delta, 0);
            if (valueh < minHeight) {
                valueh = minHeight;
            }
            if (valueh > maxHeight) {
                valueh = maxHeight;
            }
            btm.style.height = valueh + "px";
            btm.style.flexGrow = "unset";
            btm.style.flexShrink = "0";
            chatpanel.css("height", `calc(100% - (${valueh}px + 50px))`);

            lastY = e.clientY;
        });

        document.addEventListener("mouseup", (e) => {
            isResizing = false;
            document.body.style.userSelect = "auto";
        });
    })();
});
