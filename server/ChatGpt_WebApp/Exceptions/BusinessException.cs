namespace ChatGpt_WebApp.Execptions;

/// <summary>
/// 业务异常
/// </summary>
public class BusinessException : Exception
{
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="code">业务码</param>
    /// <param name="message">消息</param>
    public BusinessException(string message) : base(message)
    {

    }
}
