﻿using System.IO;
using ChatGpt_WebApp.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ChatGpt_WebApp.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[Authorize(Policy = "RequireIpAddress")]
    public class AdminController : ControllerBase
    {
        static string logsDirPath = Path.Combine(AppContext.BaseDirectory, "logs");

        public AdminController()
        {

        }

        private FileStreamResult GetStreamResult(Stream fileStream, string filename)
        {
            // 设置响应头，告诉浏览器这是一个要下载的文件
            Response.Headers.Add("Content-Disposition", "attachment; filename=" + filename);
            Response.ContentType = "application/octet-stream";

            // 将压缩文件写入 Response Body
            return new FileStreamResult(fileStream, Response.ContentType);
        }


        /// <summary>
        /// 会话日志打包下载
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="today"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult DownloadChatLog(string? ip, bool today = false)
        {
            var chatBasePath = Path.Combine(logsDirPath, "chat");
            var fullPath = chatBasePath;
            var filename = "chatlogs";

            if (!string.IsNullOrWhiteSpace(ip))
            {
                fullPath = Path.Combine(fullPath, ip.Trim());
                filename = ip;

                if (!System.IO.Directory.Exists(fullPath)) return NotFound();

                if (today)
                {
                    var newStr = DateTime.Now.ToString("yyyy-MM-dd");
                    fullPath = Path.Combine(fullPath, newStr + ".log");
                    filename += "_" + newStr;
                    filename += "_chatLogs";

                    if (!System.IO.File.Exists(fullPath)) return NotFound();
                    var filestream = System.IO.File.OpenRead(fullPath);
                    return GetStreamResult(filestream, filename + ".log");
                }
                filename += "_chatLogs";
            }

            MemoryStream stream = FileZip.ZipFile(fullPath);
            string zipFileName = string.Format("{0}.zip", filename);

            return GetStreamResult(stream, zipFileName);
        }


        /// <summary>
        /// 会话错误日志打包下载
        /// </summary>
        /// <param name="tody"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult DownloadChatErrorLog(bool today = true)
        {
            var chatBasePath = Path.Combine(logsDirPath, "chatError");
            var fullPath = chatBasePath;
            var filename = "chatErrorlogs";


            if (today)
            {
                var newStr = DateTime.Now.ToString("yyyy-MM-dd");
                fullPath = Path.Combine(fullPath, newStr + ".log");
                filename += "_" + newStr;

                if (!System.IO.File.Exists(fullPath)) return NotFound();
                var filestream = System.IO.File.OpenRead(fullPath);
                return GetStreamResult(filestream, filename + ".log");
            }

            MemoryStream stream = FileZip.ZipFile(fullPath);
            string zipFileName = string.Format("{0}.zip", filename);

            return GetStreamResult(stream, filename);
        }
    }
}
