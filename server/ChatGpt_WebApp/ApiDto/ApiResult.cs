namespace ChatGpt_WebApp.ApiDto;

public class ApiResult
{
    public static ApiResult FailResult { get => new ApiResult(false); }

    public static ApiResult SuccessResult { get => new ApiResult(true); }

    public static ApiResult SuccessFromData(object? data) { return new ApiResult(data); }

    /// <summary>
    /// 消息
    /// </summary>
    /// <value></value>
    public string? Message { get; set; }

    /// <summary>
    /// 业务码
    /// </summary>
    /// <value></value>
    public int Code { get; set; }

    /// <summary>
    /// 数据
    /// </summary>
    /// <value></value>
    public object? Data { get; set; }

    /// <summary>
    /// 是否成功
    /// </summary>
    /// <value></value>
    public bool Success { get; set; }

    public ApiResult()
    {
        this.Success = false;
    }

    public ApiResult(bool success) : this()
    {
        this.Code = success ? 0 : -1;
        this.Message = success ? "ok" : "fail";
    }

    public ApiResult(object? data)
    {
        this.Data = data;
        this.Success = true;
    }

    public ApiResult(string massage, bool success = false) : this(success)
    {
        this.Message = massage;
    }
}
