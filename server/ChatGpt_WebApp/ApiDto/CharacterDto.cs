﻿namespace ChatGpt_WebApp.ApiDto;

/// <summary>
/// Ai基本信息
/// </summary>
public class ChatAiDto
{
    /// <summary>
    /// AI名称
    /// </summary>
    public string AiName { get; set; }

    /// <summary>
    /// 程序版本
    /// </summary>
    public string? Version { get; set; }

    /// <summary>
    /// 问题最大长度
    /// </summary>
    public int MaxQuesionLength { get; set; }
    
    /// <summary>
    /// 性格模型
    /// </summary>
    public List<CharacterDto> Characters { get; set; } = new List<CharacterDto>();
}


/// <summary>
/// 性格模型
/// </summary>
public class CharacterDto
{
    /// <summary>
    /// id
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 性格AI名称
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 注解
    /// </summary>
    public string  Desc { get; set; }

    /// <summary>
    /// 图标
    /// </summary>
    public string Icon { get; set; }

    /// <summary>
    /// 头像地址
    /// </summary>
    /// <value></value>
    public string HeadSrc {get;set;}

    /// <summary>
    /// 是否应该由Ai真正的打招呼
    /// </summary>
    public bool WelcomeFromAi { get; set; }

    /// <summary>
    /// 欢迎用语列表
    /// </summary>
    public List<string> WelcomeMsgList { get; set; } = new List<string>();

    /// <summary>
    /// 清理后的会话随机列表
    /// </summary>
    public List<string> ReConversationMsgList { get; set; } = new List<string>();
}


