namespace ChatGpt_WebApp.ApiDto;

/// <summary>
/// 验证错误
/// </summary>
public class ValidationErrorDto
{
    /// <summary>
    /// 字段
    /// </summary>
    /// <value></value>
    public string Field { get; set; }

    /// <summary>
    /// 错误信息
    /// </summary>
    /// <value></value>
    public string Message { get; set; }

    public ValidationErrorDto(string field, string message)
    {
        Field = field;
        Message = message;
    }
}
