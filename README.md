<div align="center"><h1> ChatGpt-多性格自定义，wechat页面风格 </h1> </div>

<div align="center">

![MIT](https://img.shields.io/badge/license-MIT-green)
![ChatGPT.Net](https://img.shields.io/nuget/v/ChatGPT.Net?label=Nuget%20ChatGpt.net&logo=https%3A%2F%2Fapi.nuget.org%2Fv3-flatcontainer%2Fchatgpt.net%2F1.1.5%2Ficon)

</div>


## 介绍 🪴
<p>&nbsp;&nbsp;&nbsp;&nbsp;自3月份以来,由于国内访问ChatGpt遭到DNS污染，导致无法直接访问ChatGpt官网，所以我做了这个程序。</p>
<p>​&nbsp;&nbsp;&nbsp;&nbsp;这是一个支持自定义人设的ChatGpt包装程序，但是需要注意的是，它并不是免登录的，对于非官方接口模式(网站逆向)，你需要每隔一段时间魔法上网前往ChatGpt官网登录后复制Cookie 中的token 并更新到配置文件中，它才能继续工作 ;对于官方接口模式，你需要拥有官方ApiKey或者PawanOsman 的ApiKey,见<a href="https://github.com/PawanOsman/ChatGPT">https://github.com/PawanOsman/ChatGPT</a> 。由于内部API依赖第三方chatgpt代理服务器，所以偶尔会出现请求繁忙是正常的。
</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;若没有账号的，可以百度搜索 "chatgpt 账号" 获取一些共享的账号使用</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;ApiKey可以去三方网站购买</p>

## 演示 ✨

![](https://s2.loli.net/2023/03/25/R9yvqVEs8baCgnt.gif)

## 配置方法 ✍️

需要安装 [.net 7 运行环境][下载 .NET 7.0 (Linux、macOS 和 Windows) (microsoft.com)](https://dotnet.microsoft.com/zh-cn/download/dotnet/7.0),也可以在发行版直接下载整合包(集成了运行环境,缺点是文件体积较大)

### 非官方接口模式(暂时无法使用)

1. 开启魔法上网 去 [ChatGpt](https://chat.openai.com/chat) 登录后复制token(如图)，复制到 [appsetting.template.json](./server/ChatGpt_WebApp/appsettings.template.json) 的 TokenPool.SessionToken 节点

   ![image.png](https://s2.loli.net/2023/03/25/eQy7ZP6norETKu5.png)

2. 编辑 [appsetting.template.json](./server/ChatGpt_WebApp/appsettings.template.json) 编辑 Characters 节点定义想要的性格模型即可，注意性格ID不能重复

3. 将 appsetting.template.json 重命名为 appsetting.json

4. 找到 ChatGpt_WebApp.exe ，双击run，然后打开浏览器访问

### 官方接口模式(默认选项)
注意📢: 目前官方key默认使用的是第三方免费代理服务器，风险请自行甄别,要修改见配置文件
1. 获取APIKEY，可用的ApiKey有两种，分别为官方的key:"sk-xxxx" 和 PawanOsman的key:"pk-xxxx"见
 https://github.com/PawanOsman/ChatGPT ,程序将根据前缀自动选择服务器

2. 将apikey 填入 appsettings.json 中的 TokenPol中

## 免责声明 📝
本程序不会上传,搜集使用者的ApiKey，唯一的安全隐患您只需甄别安全的第三方代理服务器，无法保证它们不搜集,泄露你的ApiKey。所以尽量使用私人代理服务器。若您的ApiKey在使用过程中泄露造成您的资金损失。与本人无关。


## 鸣谢 📢
感谢 [PawanOsman](https://github.com/PawanOsman) 提供的 [ChatGPT.Net](https://github.com/PawanOsman/ChatGPT.Net) Nuget包，并免费开放代理服务
